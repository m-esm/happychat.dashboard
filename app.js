﻿const express = require('express');
const app = express();
const path = require('path');

global.__appRoot = path.normalize(__dirname);

app.get(/^\/([a-z]|[0-9]|_){0,}$/igm, function (req, res) {

    console.log(req.url);
    res.sendFile('./www/index.html', { root: __appRoot });

});

app.use(express.static('www'));


app.listen(3000, function () {
    console.log('Dashboard 3000!')
});

