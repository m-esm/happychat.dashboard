﻿happyDashboard.controller('dashboardController', function ($scope, $chatadmin_socket, $http, $rootScope) {
  
    $scope.dashboards = null;
    $scope.newDashboard = { token: $rootScope.user.token};



    $scope.createDashboard = function () {

        $http.post(apiUrl + '/dashboard/create', $scope.newDashboard ).then(function (res) {

            $rootScope.dashboard = res.data;
            if (!$rootScope.dashboards)
                $rootScope.dashboards = [res.data];


        }, function (err) {

        });



    };

    $scope.setDashboard = function (_id) {


        $chatadmin_socket.emit('switchDashboard', _id);


        $http.post(apiUrl + '/account/dashboards', {
            token: $rootScope.user.token
        }).then(function (res) {

            var dashboards = res.data;

            var _dash = _.findWhere(dashboards, { '_id': _id });

            $rootScope.dashboard = _dash;
            localStorage.setItem('dashboard', JSON.stringify(_dash));
            switchToDashboard = _dash._id;

        }, function (err) {

        });



    };

    $http.post(apiUrl + '/account/dashboards', {
        token: $rootScope.user.token
    }).then(function (res) {

        $scope.dashboards = res.data;

        if (!$rootScope.dashboard)
            if (res.data[0]) {
                $rootScope.dashboard = res.data[0];
                localStorage.setItem('dashboard', JSON.stringify( res.data[0]));
                switchToDashboard = $rootScope.dashboard._id;
            }


    }, function (err) {

    });

});