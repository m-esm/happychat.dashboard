happyDashboard.controller('dashboardController', function ($scope, $chatadmin_socket, $http, $rootScope) {
  
    $scope.dashboards = null;
    $scope.newDashboard = { token: $rootScope.user.token};



    $scope.createDashboard = function () {

        $http.post(apiUrl + '/dashboard/create', $scope.newDashboard ).then(function (res) {

            $rootScope.dashboard = res.data;
            if (!$rootScope.dashboards)
                $rootScope.dashboards = [res.data];


        }, function (err) {

        });



    };

    $scope.setDashboard = function (_id) {


        $chatadmin_socket.emit('switchDashboard', _id);


        $http.post(apiUrl + '/account/dashboards', {
            token: $rootScope.user.token
        }).then(function (res) {

            var dashboards = res.data;

            var _dash = _.findWhere(dashboards, { '_id': _id });

            $rootScope.dashboard = _dash;
            localStorage.setItem('dashboard', JSON.stringify(_dash));
            switchToDashboard = _dash._id;

        }, function (err) {

        });



    };

    $http.post(apiUrl + '/account/dashboards', {
        token: $rootScope.user.token
    }).then(function (res) {

        $scope.dashboards = res.data;

        if (!$rootScope.dashboard)
            if (res.data[0]) {
                $rootScope.dashboard = res.data[0];
                localStorage.setItem('dashboard', JSON.stringify( res.data[0]));
                switchToDashboard = $rootScope.dashboard._id;
            }


    }, function (err) {

    });

});

happyDashboard.controller('authController', function ($scope, $http, $rootScope, $location, $chatadmin_socket) {

    $scope.switch = function (to) {

        $location.hash(to);

    };


    $scope.registerModel = {};




    $scope.register = function () {
    
        $http.post(apiUrl + '/account/register', $scope.registerModel).then(function (res) {

          

                swal({
                    title: '',
                    text: 'ثبت نام شما موفقیت آمیز بود لطفا وارد شوید .',
                    type: 'success'
                }).then(function () {

                    window.location = "/#!/auth";
                  
                    }, function (diss) { });

            

        }, function (err) {

            if (err.data)
                swal.apiError(err.data);
            else
                swal.ajaxError(err);

        });

    };

    $scope.showForm = $location.hash() ? $location.hash() : 'login';


    $scope.login = function () {

        //$scope.$watch($location.hash(), function (newVal) {

        //    $scope.showForm = newVal;

        //});


        $http.post(apiUrl + '/account/login', { email: $scope.email, password: $scope.password }).then(function (res) {


            localStorage.setItem("user", JSON.stringify(res.data));
            $rootScope.user = res.data;

            if ($rootScope.user)
                $chatadmin_socket.emit('auth', $rootScope.user.token);

            window.location = '/#!/dashboard';


        }, function (res) {

            $scope.error = res.data.msg;



        });


    };

});
happyDashboard.controller('registerController', function ($scope) {


});
happyDashboard.filter('orderChatVisitors', function () {
    // custom value function for sorting


    function getLastMessage(item) {

        if (item.chats || item.lastMessages)
            return item.chats ? item.chats[item.chats.length - 1] : item.lastMessages[item.lastMessages.length - 1];
        else
            return undefined;
    }

    function myValueFunction(item) {

        if (getLastMessage(item) != undefined) {
            var timestamp = moment(getLastMessage(item).insertDate).locale('en').format('x');
            return timestamp;
        } else {


        }
    }

    return function (obj) {

        if (obj) {

            var array = [];
            Object.keys(obj).forEach(function (key) {
                // inject key into each object so we can refer to it from the template
                obj[key].name = key;
                array.push(obj[key]);
            });
            // apply a custom sorting function
            array.sort(function (a, b) {
                return myValueFunction(b) - myValueFunction(a);
            });
            return array;
        } else {
            return false;
        }
    };
});

happyDashboard.controller('chatsController', function ($scope, $chatadmin_socket, $http, $rootScope, $timeout) {



    $scope.getLastMessage = function (item) {

        if (item.lastMessages || item.chats)
            return item.chats ? item.chats[item.chats.length - 1] : item.lastMessages[item.lastMessages.length - 1];
        else
            return undefined;
    }
    $chatadmin_socket.emit('getVisitors');

    $chatadmin_socket.on('visitors', function (res) {

        $scope.visitors = res.data;
        //  console.log(res);


    });


    $chatadmin_socket.on('newVisit', function (visit) {



        if (visit.__v == 0) {
            console.log('new Visit', visit);

            var visitorIndex = _.findIndex($scope.visitors, {
                _id: visit.visitor
            });

            if (!$scope.visitors[visitorIndex].visits)
                $scope.visitors[visitorIndex].visits = [];

            $scope.visitors[visitorIndex].visits.push(visit);

            if ($scope.visitor._id == visit.visitor)
                $scope.visitor = $scope.visitors[visitorIndex];

            $scope.$apply();
        }

    });



    $scope.sendMessage = function (msg) {

        document.getElementById('msgText').value = '';

        if ($scope.visitor) {
            msg.visitor = $scope.visitor._id;

            $chatadmin_socket.emit('sendMessage', msg);
        }

    };

    $chatadmin_socket.on('seen', function (seenById) {


        var visitorIndex = _.findIndex($scope.visitors, {
            _id: typerId
        });


        $scope.visitors[visitorIndex].chats = _.map($scope.visitors[visitorIndex].chats, function (item) {

            if (!item.fromVisitor) {

                item.seen = true;
                item.seenDate = new Date();
            }

            return item;
        });

        $scope.$apply();

    });

    $chatadmin_socket.on('typing', function (typerId, typingWhat) {

        var visitorIndex = _.findIndex($scope.visitors, {
            _id: typerId
        });


        $scope.visitors[visitorIndex].typing = typingWhat;

        $scope.$apply();

    });

    $chatadmin_socket.on('typingEnd', function (typerId, typingWhat) {

        var visitorIndex = _.findIndex($scope.visitors, {
            _id: typerId
        });

        $scope.visitors[visitorIndex].typing = false;

        $scope.$apply();

    });

    $chatadmin_socket.on('newVisitor', function (doc) {

        if (doc.__v == 0)
            $scope.visitors.push(doc);
        else {


            $scope.visitors = _.map($scope.visitors, function (item) {


                if (item._id == doc._id) {
                    if (item.chats)
                        doc.chats = item.chats;

                    item = doc;

                    if ($scope.visitor._id == item._id)
                        $scope.visitor = doc;

                }
                return item;

            });

            $scope.$apply();
        }

    });

    $chatadmin_socket.on('newMessage', function (doc) {

        var visitorIndex = _.findIndex($scope.visitors, {
            _id: doc.visitor._id
        });

        if (!$scope.visitors[visitorIndex].chats)
            $scope.visitors[visitorIndex].chats = [];

        $scope.visitors[visitorIndex].chats.push(doc);
        $scope.$apply();

        if ($scope.visitor)
            if (doc.visitor._id == $scope.visitor._id) {




                setTimeout(function () {
                    document.getElementById('chathistory').scrollTop = document.getElementById('chathistory').scrollHeight + 100;

                }, 100);
            }


    });




    $scope.showVisitor = function (visitor) {

        $chatadmin_socket.emit('getChats', visitor._id);

        $scope.visitor = visitor;

        $chatadmin_socket.on('chats', function (res) {

            $scope.visitor.chats = res.data;
            $scope.$broadcast('rebuild:me');
            setTimeout(function () {
                document.getElementById('chathistory').scrollTop = document.getElementById('chathistory').scrollHeight + 100;

            }, 100);

        });

    };



});