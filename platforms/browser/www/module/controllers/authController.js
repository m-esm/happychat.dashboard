﻿
happyDashboard.controller('authController', function ($scope, $http, $rootScope, $location, $chatadmin_socket) {

    $scope.switch = function (to) {

        $location.hash(to);

    };


    $scope.registerModel = {};




    $scope.register = function () {
    
        $http.post(apiUrl + '/account/register', $scope.registerModel).then(function (res) {

          

                swal({
                    title: '',
                    text: 'ثبت نام شما موفقیت آمیز بود لطفا وارد شوید .',
                    type: 'success'
                }).then(function () {

                    window.location = "/#!/auth";
                  
                    }, function (diss) { });

            

        }, function (err) {

            if (err.data)
                swal.apiError(err.data);
            else
                swal.ajaxError(err);

        });

    };

    $scope.showForm = $location.hash() ? $location.hash() : 'login';


    $scope.login = function () {

        //$scope.$watch($location.hash(), function (newVal) {

        //    $scope.showForm = newVal;

        //});


        $http.post(apiUrl + '/account/login', { email: $scope.email, password: $scope.password }).then(function (res) {


            localStorage.setItem("user", JSON.stringify(res.data));
            $rootScope.user = res.data;

            if ($rootScope.user)
                $chatadmin_socket.emit('auth', $rootScope.user.token);

            window.location = '/#!/dashboard';


        }, function (res) {

            $scope.error = res.data.msg;



        });


    };

});