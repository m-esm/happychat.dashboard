﻿let happyDashboard = angular.module('dashboard', ["ngRoute", "ngAnimate", 'btford.socket-io']);



const apiUrl = 'http://192.168.2.200:2121/api';

swal.setDefaults({
    confirmButtonText: 'بسیار خب',
});

moment.locale('fa');



swal.ajaxError = function (err) {

    console.log(err);

    swal({
        title: 'خطایی رخ داده است لطفا پشتیبانی سایت را مطلع سازید .',
        text: '',
        type: 'warning'
    }).then(function () {



    }, function (diss) { });



};


swal.apiError = function (err) {

    var msg = '<ul>';
    err.forEach(function (item, index) {
        msg += '<li>' + item.msg + '</li>';
    });
    msg += '</ul>';

    swal({
        title: '',
        html : msg,
        type: 'error'
    }).then(function () {




    }, function (diss) { });



};



AOS.init();


var app = {
    // Application Constructor
    initialize: function () {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function () {

        alert(device.model + ' ' + device.platform + ' ' + device.version);

    }
};

app.initialize();

happyDashboard.factory('$chatadmin_socket', function (socketFactory) {
   
    var myIoSocket = io.connect("http://192.168.2.200:2121/chatAdmin", { 'force new connection': true});
    myIoSocket.on('disconnect', function () {
        console.log("Connection disconnected");
    });
    mySocket = socketFactory({
        ioSocket: myIoSocket
    });

    return mySocket;
});



happyDashboard.config(function ($routeProvider, $locationProvider) {

    $routeProvider.
        when('/auth', {
            templateUrl: 'views/auth.html',
            controller: 'authController'
        })
        .when('/register', {
            templateUrl: 'views/register.html',
            controller: 'registerController'
        })
        .when('/dashboard', {
            templateUrl: 'views/dashboard.html',
            controller: 'dashboardController',
            data: {
                requireLogin: true
            }
        }).when('/chats', {
            templateUrl: 'views/chats.html',
            controller: 'chatsController',
            data: {
                requireLogin: true
            }
        });




    //$locationProvider.html5Mode(false);

});

happyDashboard.run(function ($location, $rootScope, $timeout, $http, $chatadmin_socket) {

    $rootScope._ = _;
    $rootScope.moment = moment;
    $rootScope.toggle = function () {
        $rootScope.showSidebar = $rootScope.showSidebar == true ? false : true;
    };

    $rootScope.logout = function () {

        $rootScope.user = null;
        $location.path('/auth');
        localStorage.clear();



    };

    $timeout(function () {

        $rootScope.loaded = true;
    }, 500);

    //$chatadmin_socket.on('newVisitor', function (doc) {
    //    console.log('newVisitor',doc);
    //});
    //$chatadmin_socket.on('newVisit', function (doc) {
    //    console.log('newVisit',doc);
    //});
    //$chatadmin_socket.on('newMessage', function (doc) {
    //    console.log('newChat',doc);
    //});

  

    $rootScope.user = JSON.parse(localStorage.getItem('user'));
    if ($rootScope.user)
        $chatadmin_socket.emit('auth', $rootScope.user.token);

    $rootScope.dashboard = JSON.parse(localStorage.getItem('dashboard'));

    if ($rootScope.dashboard)
        $chatadmin_socket.emit('dashboard', $rootScope.dashboard._id);


    $rootScope.$on("$routeChangeStart", function (event, next, current) {
        $rootScope.showSidebar = false;
        var requireLogin = false;

        console.log($location.path());


        if (next.data)
            requireLogin = next.data.requireLogin;

      

        if (requireLogin && !localStorage.user) {
            // no logged user, we should be going to #login
            if (next.templateUrl == "views/auth.html") {
               

                // already going to #login, no redirect needed
            } else {
                // not going to #login, we should redirect now
                $location.path("/auth");
            }
        }
    });


    if ($location.path() == "/" || !$location.path())
        $location.path('/dashboard');

    if ($rootScope.user)
    if (!$rootScope.dashboard) 
        $location.path('/dashboard');



    $rootScope.location = $location;
    $rootScope.title = $location.path();



});